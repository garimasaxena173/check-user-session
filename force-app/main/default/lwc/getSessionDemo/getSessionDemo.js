import { LightningElement } from 'lwc';
import {
    getSession
} from 'c/util';


export default class GetSessionDemo extends LightningElement {

    connectedCallback() {
        getSession(this, response=>{
            //continue working
        });
    }
}

