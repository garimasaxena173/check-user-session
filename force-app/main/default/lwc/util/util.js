import { LightningElement } from 'lwc';
import getSessionId from "@salesforce/apex/SessionDemo.getSessionId";

export default class Util extends LightningElement {}

export function getSession(ctx, callback) {
    getSessionId({}).then(data => {
        if (data) {
            callback({
                data: data
            });
        } else {
            showInfoMessage(ctx, 'Session Expired! Please login again');
        }
    }).catch(error => {
        callback({
            error: error
        });
    })
}