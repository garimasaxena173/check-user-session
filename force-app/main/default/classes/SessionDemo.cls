public with sharing class SessionDemo {
    
    @AuraEnabled
    public static String getSessionId() {
        String sessionId = UserInfo.getSessionId();
        return sessionId;
    }
}
